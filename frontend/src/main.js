// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from "axios";
import routes from "./routes"
import VueRouter from 'vue-router';

Vue.config.productionTip = false
let axiosOptions = {}
if (process.env.NODE_ENV === 'development') {
  axiosOptions.baseURL = 'http://127.0.0.1:8000';
}
Vue.prototype.$axios = axios.create(axiosOptions)
Vue.use(VueRouter);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: new VueRouter(
    {
      routes,
      mode: 'hash',
    }
  ),
  components: {App},
  template: '<App/>'
})
