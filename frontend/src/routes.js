import Calculator from './components/Calculator'
import List from './components/List'

export default [
  {
    path: '/',
    component: Calculator,
  },
  {
    path: '/list',
    component: List,
  }
];
