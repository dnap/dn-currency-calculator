<?php


namespace App\ExchangeRateSource\Parser;


use App\Dto\CurrencyPair;
use App\Exception\InvalidDataFormatException;
use App\Factory\CurrencyFactory;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class CoinDeskParser implements ParserInterface
{
    private const SOURCE_CURRENCY = 'BTC';
    private const TARGET_CURRENCY = 'USD';

    public function __construct(private CurrencyFactory $currencyFactory)
    {
    }

    public function parse(string $data): array
    {
        $array = $this->decode($data);

        $time = 0;
        $rate = 0;
        foreach ($array['bpi'] as $currentDate => $currentRate) {
            $currentTime = strtotime($currentDate);
            if ($currentTime > $time) {
                $time = $currentTime;
                $rate = $currentRate;
            }
        }
        if ($rate <= 0) {
            throw new InvalidDataFormatException('Rate is invalid: ' . $rate);
        }
        return [
            new CurrencyPair(
                $this->currencyFactory->create(self::SOURCE_CURRENCY),
                $this->currencyFactory->create(self::TARGET_CURRENCY),
                $rate
            ),
        ];
    }

    private function decode(string $data): array
    {
        $decoder = new JsonDecode([JsonDecode::ASSOCIATIVE => true]);
        return $decoder->decode($data, JsonEncoder::FORMAT);
    }
}