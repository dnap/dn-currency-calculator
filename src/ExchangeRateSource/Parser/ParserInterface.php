<?php


namespace App\ExchangeRateSource\Parser;


use App\Service\Dto\CurrencyPair;

interface ParserInterface
{
    /**
     * @return CurrencyPair[]
     */
    public function parse(string $data): array;
}