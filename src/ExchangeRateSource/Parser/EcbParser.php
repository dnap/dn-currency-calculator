<?php


namespace App\ExchangeRateSource\Parser;


use App\Dto\Currency;
use App\Dto\CurrencyPair;
use App\Exception\InvalidDataFormatException;
use App\Exception\InvalidXmlException;
use App\Factory\CurrencyFactory;

class EcbParser implements ParserInterface
{
    const MAIN_CURRENCY_CODE = 'EUR';
    private Currency $mainCurrency;

    public function __construct(private CurrencyFactory $currencyFactory)
    {
        $this->mainCurrency = $currencyFactory->create(self::MAIN_CURRENCY_CODE);
    }

    /**
     * @inheritDoc
     */
    public function parse(string $data): array
    {
        $xml = simplexml_load_string($data);
        if ($xml === false) {
            throw new InvalidXmlException('Can\'t parse the xml: ' . $data);
        }
        $result = [];
        foreach ($xml->Cube->Cube->Cube as $row) {
            $code = (string)$row['currency'];
            $rate = (float)$row['rate'];
            if (strlen($code) !== 3) {
                throw new InvalidDataFormatException('Currency code is invalid: ' . $code);
            }
            if ($rate <= 0) {
                throw new InvalidDataFormatException('Rate is invalid: ' . $rate);
            }

            $result[] = new CurrencyPair($this->mainCurrency, $this->currencyFactory->create($code), $rate);
        }
        return $result;
    }
}