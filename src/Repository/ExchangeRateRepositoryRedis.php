<?php


namespace App\Repository;


use App\Dto\CurrencyPair;
use App\Factory\CurrencyPairFactory;
use Predis\Client;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ExchangeRateRepositoryRedis implements ExchangeRateRepositoryInterface
{
    private const KEY = 'exchangeRate';
    private EncoderInterface $encoder;
    private DecoderInterface $decoder;

    public function __construct(private Client $redis, private CurrencyPairFactory $currencyPairFactory)
    {
        $this->encoder = new JsonEncode();
        $this->decoder = new JsonDecode([JsonDecode::ASSOCIATIVE => true]);
    }

    public function updatePairs(string $id, CurrencyPair ...$pairs): void
    {
        $this->redis->hset(self::KEY, $id, $this->encoder->encode($pairs, JsonEncoder::FORMAT));
    }

    /**
     * @inheritDoc
     */
    public function getAllPairs(): array
    {
        $result = [];
        foreach ($this->redis->hvals(self::KEY) as $pairs) {
            $pairs = $this->decoder->decode($pairs, JsonEncoder::FORMAT);
            $pairs = $this->createCurrencyPairs($pairs);
            if (!empty($pairs))
                array_push($result, ...$pairs);
        }

        return $result;
    }

    private function createCurrencyPairs($pairs): array
    {
        $result = [];
        foreach ($pairs as $pairData) {
            $result[] = $this->currencyPairFactory->createFromArray($pairData);
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getPairsGroupBySources(): array
    {
        $keys = $this->redis->hkeys(self::KEY);
        $values = [];
        foreach ($this->redis->hvals(self::KEY) as $pairs) {
            $pairs = $this->decoder->decode($pairs, JsonEncoder::FORMAT);
            $values[] = $this->createCurrencyPairs($pairs);
        }

        return array_combine($keys, $values);
    }
}