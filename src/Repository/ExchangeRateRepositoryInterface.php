<?php


namespace App\Repository;


use App\Dto\CurrencyPair;

interface ExchangeRateRepositoryInterface
{
    public function updatePairs(string $id, CurrencyPair ...$pairs): void;

    /**
     * @return CurrencyPair[]
     */
    public function getAllPairs(): array;

    /**
     * @return CurrencyPair[][]
     */
    public function getPairsGroupBySources(): array;
}