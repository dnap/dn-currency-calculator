<?php


namespace App\Transport;


use GuzzleHttp\Client;

class HttpGetTransport implements TransportInterface
{
    private Client $client;

    public function __construct(private string $url, Client $client = null)
    {
        if ($client === null) {
            $client = $this->createClient();
        }
        $this->client = $client;
    }

    protected function createClient(): Client
    {
        return new Client();
    }

    public function grab(): string
    {
        $response = $this->client->get($this->url);
        return (string)$response->getBody();
    }
}