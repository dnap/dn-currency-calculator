<?php


namespace App\Transport;


interface TransportInterface
{
    public function grab(): string;
}