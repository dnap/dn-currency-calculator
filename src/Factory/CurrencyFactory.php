<?php


namespace App\Factory;


use App\Dto\Currency;

class CurrencyFactory
{
    private array $cache;

    // Todo move to config
    private $defaultPrecision = 2;
    private $precision = [
        'BTC' => 8, // Satoshi
    ];

    public function create(string $code): Currency
    {
        $code = mb_strtoupper($code);
        if (!isset($this->cache[$code])) {
            $this->cache[$code] = new Currency($code, $this->precision[$code] ?? $this->defaultPrecision);
        }
        return $this->cache[$code];
    }
}