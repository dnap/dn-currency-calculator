<?php


namespace App\Factory;


use App\Dto\CurrencyPair;

class CurrencyPairFactory
{
    public function __construct(private CurrencyFactory $currencyFactory)
    {
    }

    public function createFromArray(array $data): CurrencyPair
    {
        return new CurrencyPair(
            $this->currencyFactory->create($data['currencySource']['code']),
            $this->currencyFactory->create($data['currencyTarget']['code']),
            $data['exchangeRate']
        );
    }
}