<?php


namespace App\Grabber;


use App\Exception\InvalidCurrencySourceException;
use App\ExchangeRateSource\Parser\CoinDeskParser;
use App\ExchangeRateSource\Parser\EcbParser;
use App\Factory\CurrencyFactory;
use App\Transport\HttpGetTransport;

class ExchangeRateSourceFactory
{
    private const ECB = 'ecb';
    private const COINDESK = 'conidesk';

    public function __construct(private CurrencyFactory $currencyFactory)
    {
    }


    public function createEcb(): ExchangeRateGrabber
    {
        return $this->get(self::ECB);
    }

    public function get(string $sourceName): ExchangeRateGrabber
    {
        // todo move to config
        switch ($sourceName) {
            case self::ECB:
                return new ExchangeRateGrabber(
                    $sourceName,
                    new HttpGetTransport('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'),
                    new EcbParser($this->currencyFactory)
                );
            case self::COINDESK:
                return new ExchangeRateGrabber(
                    $sourceName,
                    new HttpGetTransport('https://api.coindesk.com/v1/bpi/historical/close.json'),
                    new CoinDeskParser($this->currencyFactory)
                );
        }
        throw new InvalidCurrencySourceException('Invalid currency source: ' . $sourceName);
    }

    public function createCoinDesk(): ExchangeRateGrabber
    {
        return $this->get(self::COINDESK);
    }
}