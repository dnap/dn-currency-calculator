<?php


namespace App\Grabber;


use App\Dto\CurrencyPair;

interface ExchangeRateGrabberInterface
{
    /**
     * @return CurrencyPair[]
     */
    public function getPairs(): array;

    public function getId(): string;
}