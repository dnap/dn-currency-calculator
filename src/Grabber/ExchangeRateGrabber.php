<?php


namespace App\Grabber;


use App\Dto\CurrencyPair;
use App\ExchangeRateSource\Parser\ParserInterface;
use App\Transport\TransportInterface;

class ExchangeRateGrabber implements ExchangeRateGrabberInterface
{
    public function __construct(
        private string $id,
        private TransportInterface $transport,
        private ParserInterface $parser)
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return CurrencyPair[]
     */
    public function getPairs(): array
    {
        $content = $this->transport->grab();
        return $this->parser->parse($content);
    }
}