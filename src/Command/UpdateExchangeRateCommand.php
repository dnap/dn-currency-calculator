<?php


namespace App\Command;

use App\Service\ExchangeRateUpdateService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateExchangeRateCommand extends Command
{
    protected static $defaultName = 'app:exchangeRate:update';

    public function __construct(private ExchangeRateUpdateService $service, string $name = null)
    {
        parent::__construct($name);
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->do();
//        var_dump($this->container->findTaggedServiceIds('strategy'));
        // ...

        return Command::SUCCESS;
    }
}