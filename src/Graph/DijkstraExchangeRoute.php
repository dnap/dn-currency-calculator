<?php


namespace App\Graph;


use App\Dto\Currency;
use App\Dto\CurrencyPair;
use App\Exception\RouteNotFoundException;
use App\Factory\CurrencyFactory;
use GraphDS\Algo\Dijkstra;
use GraphDS\Graph\UndirectedGraph;

class DijkstraExchangeRoute implements ExchangeRouteInterface
{
    public function __construct(private CurrencyFactory $currencyFactory)
    {
    }

    /**
     * @inheritDoc
     */
    public function findPath(Currency $startCurrency, Currency $endCurrency, CurrencyPair ...$pairs): array
    {
        $graph = $this->createGraph($pairs);

        $algo = new Dijkstra($graph);
        $algo->run($startCurrency->getCode());
        $route = $algo->get($endCurrency->getCode())['path'];

        if (empty($route))
            throw new RouteNotFoundException('Not found path from ' . $startCurrency->getCode() . ' to ' . $endCurrency->getCode());

        return array_map(fn($code) => $this->currencyFactory->create($code), $route);
    }

    private function createGraph(array $pairs): UndirectedGraph
    {
        $graph = new UndirectedGraph();

        foreach ($pairs as $pair) {
            $from = $pair->getCurrencySource()->getCode();
            $to = $pair->getCurrencyTarget()->getCode();
            $graph->addVertex($from);
            $graph->addVertex($to);
            $graph->addEdge($from, $to);
        }
        return $graph;
    }
}