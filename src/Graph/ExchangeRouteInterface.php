<?php


namespace App\Graph;


use App\Dto\Currency;
use App\Dto\CurrencyPair;

interface ExchangeRouteInterface
{
    /**
     * @return Currency[]
     */
    public function findPath(Currency $startCurrency, Currency $endCurrency, CurrencyPair ...$pairs): array;
}