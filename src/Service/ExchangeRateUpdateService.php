<?php


namespace App\Service;


use App\Grabber\ExchangeRateGrabberInterface;
use App\Repository\ExchangeRateRepositoryInterface;
use Psr\Log\LoggerInterface;

class ExchangeRateUpdateService
{
    public function __construct(
        private $exchangeRateServices,
        private ExchangeRateRepositoryInterface $repository,
        private LoggerInterface $logger)
    {
    }

    public function do(): void
    {
        /** @var ExchangeRateGrabberInterface $exchangeRateService */
        foreach ($this->exchangeRateServices as $exchangeRateService) {
            try {
                $pairs = $exchangeRateService->getPairs();
                $this->repository->updatePairs($exchangeRateService->getId(), ...$pairs);
            } catch (\Throwable $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
            }
        }
    }
}