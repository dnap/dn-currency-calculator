<?php


namespace App\Service;


use App\Dto\Currency;
use App\Dto\CurrencyPair;
use App\Exception\NotFoundExchangeRateException;
use App\Factory\CurrencyPairFactory;
use App\Graph\ExchangeRouteInterface;
use App\Repository\ExchangeRateRepositoryInterface;

class ExchangeRateService
{
    const USER_RATE_SOURCE = 'user';

    public function __construct(
        private ExchangeRateRepositoryInterface $repository,
        private ExchangeRouteInterface $router,
        private CurrencyPairFactory $currencyPairFactory)
    {
    }

    /**
     * @result Currency[]
     */
    public function getAllCurrencies(): array
    {
        $pairs = $this->repository->getAllPairs();
        $currencies = [];
        foreach ($pairs as $pair) {
            array_push($currencies,
                $pair->getCurrencyTarget(),
                $pair->getCurrencySource()
            );
        }

        return $this->uniqueCurrencies($currencies);
    }

    /**
     * @return Currency[]
     * @var Currency[] $currency
     */
    private function uniqueCurrencies(array $currency): array
    {
        $currencyCodes = [];
        return array_filter($currency, function (Currency $currency) use (&$currencyCodes) {
            if (array_search($currency->getCode(), $currencyCodes))
                return false;
            $currencyCodes[] = $currency->getCode();
            return true;
        });
    }

    public function convert(Currency $source, Currency $target, float $amount): float
    {
        if ($source === $target)
            return $amount;

        $pairs = $this->repository->getAllPairs();
        $path = $this->router->findPath($source, $target, ...$pairs);
        $prevCurrency = reset($path);
        for ($i = 1; $i < count($path); $i++) {
            $curRate = $this->getExchangeRate($prevCurrency, $path[$i], $pairs);
            $amount *= $curRate;
            $prevCurrency = $path[$i];
        }

        return $amount;
    }

    private function getExchangeRate(Currency $source, Currency $target, array $pairs): float
    {
        foreach ($pairs as $pair) {
            if ($pair->getCurrencySource() === $source && $pair->getCurrencyTarget() === $target) {
                return $pair->getExchangeRate();
            }
            if ($pair->getCurrencySource() === $target && $pair->getCurrencyTarget() === $source) {
                return 1 / $pair->getExchangeRate();
            }
        }
        throw new NotFoundExchangeRateException('Not found exchange rate ' . $source->getCode() . ' => ' . $target->getCode());
    }

    public function getAllPairs(): array
    {
        return $this->repository->getAllPairs();
    }

    public function upsertPair(CurrencyPair $pair): void
    {
        $pairsMap = $this->repository->getPairsGroupBySources();
        foreach ($pairsMap as $source => $pairs) {
            foreach ($pairs as $k => $curPair) {
                if ($pair->getCurrencySource() === $curPair->getCurrencySource() && $pair->getCurrencyTarget() === $curPair->getCurrencyTarget()) {
                    $pairs[$k] = $pair;
                    $this->repository->updatePairs($source, ...$pairs);
                    return;
                }
            }
        }
        if (!isset($pairsMap[self::USER_RATE_SOURCE])) {
            $pairsMap[self::USER_RATE_SOURCE] = [];
        }
        $pairsMap[self::USER_RATE_SOURCE][] = $pair;
        $this->repository->updatePairs(self::USER_RATE_SOURCE, ...$pairsMap[self::USER_RATE_SOURCE]);
    }
}