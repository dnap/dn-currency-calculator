<?php


namespace App\Controller\Api;


use App\Factory\CurrencyFactory;
use App\Service\ExchangeRateService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ConvertAction
{
    public function __construct(private ExchangeRateService $exchangeRateService, private CurrencyFactory $currencyBuilder)
    {
    }

    #[Route('/api/currencies/{source}/{target}/{amount}', methods: ['GET'], requirements: ['source' => '[A-Z]{3}', 'target' => '[A-Z]{3}', 'amount' => '\d+(\.\d+|)'])]
    public function invoke(string $source, string $target, float $amount): JsonResponse
    {
        $targetCurrency = $this->currencyBuilder->create($target);
        $result = $this->exchangeRateService->convert(
            $this->currencyBuilder->create($source),
            $targetCurrency,
            $amount
        );
        return new JsonResponse(['amount' => $result, 'precision' => $targetCurrency->getPrecision()]);
    }
}