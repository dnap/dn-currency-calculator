<?php


namespace App\Controller\Api;


use App\Service\ExchangeRateService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class GetCurrenciesAction
{
    public function __construct(private ExchangeRateService $exchangeRateService)
    {
    }

    #[Route('/api/currencies/', methods: ['GET'])]
    public function invoke(): JsonResponse
    {
        return new JsonResponse(array_values($this->exchangeRateService->getAllCurrencies()));
    }
}