<?php


namespace App\Controller\Api;


use App\Dto\CurrencyPair;
use App\Factory\CurrencyFactory;
use App\Service\ExchangeRateService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PairsController
{
    public function __construct(private ExchangeRateService $exchangeRateService, private CurrencyFactory $currencyFactory)
    {
    }

    #[Route('/api/pairs/', methods: ['GET'])]
    public function get(): JsonResponse
    {
        return new JsonResponse(['pairs' => $this->exchangeRateService->getAllPairs()]);
    }

    #[Route('/api/pairs/{source}/{target}', methods: ['PUT'], requirements: ['source' => '[A-Z]{3}', 'target' => '[A-Z]{3}', 'rate' => '\d+(\.\d+|)'])]
    public function upsert(string $source, string $target,Request $request): JsonResponse
    {
        $this->exchangeRateService->upsertPair(
            new CurrencyPair(
                $this->currencyFactory->create($source),
                $this->currencyFactory->create($target),
                (float)$request->get('rate')
            )
        );
        return new JsonResponse(['success' => true]);
    }
}