<?php


namespace App\Dto;


use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class CurrencyPair implements \JsonSerializable
{
    public function __construct(
        private Currency $currencySource,
        private Currency $currencyTarget,
        private float $exchangeRate)
    {
    }

    public function getCurrencySource(): Currency
    {
        return $this->currencySource;
    }

    public function getCurrencyTarget(): Currency
    {
        return $this->currencyTarget;
    }

    public function getExchangeRate(): float
    {
        return $this->exchangeRate;
    }

    #[ArrayShape(['currencySource' => "\App\Dto\Currency", 'currencyTarget' => "\App\Dto\Currency", 'exchangeRate' => "float"])]
    public function jsonSerialize(): array
    {
        return [
            'currencySource' => $this->currencySource,
            'currencyTarget' => $this->currencyTarget,
            'exchangeRate' => $this->exchangeRate,
        ];
    }
}