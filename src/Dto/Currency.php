<?php


namespace App\Dto;


class Currency implements \JsonSerializable
{
    public function __construct(private string $code, private int $precision)
    {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getPrecision(): int
    {
        return $this->precision;
    }

    public function jsonSerialize()
    {
        return ['code' => $this->code];
    }

    public function __toString(): string
    {
        return $this->code;
    }
}