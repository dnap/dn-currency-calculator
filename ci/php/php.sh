#!/bin/bash
set -ex

composer install --no-interaction
./bin/console app:exchangeRate:update
symfony server:start
