<?php

namespace App\Tests\ExchangeRateSource\Parser;

use App\ExchangeRateSource\Parser\CoinDeskParser;
use PHPUnit\Framework\TestCase;

class CoinDeskParserTest extends TestCase
{
    public function testParse()
    {
        $sample = <<<'JSON'
{"bpi":{
    "2021-01-14":39138.075,
    "2021-01-15":36770.0433
    },
    "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. BPI value data returned as USD.",
    "time":{"updated":"Jan 16, 2021 00:03:00 UTC","updatedISO":"2021-01-16T00:03:00+00:00"}}
JSON;
        $parser = new CoinDeskParser();
        $pairs = $parser->parse($sample);
        $this->assertCount(1, $pairs);
        $pair = $pairs[0];
        $this->assertEquals('BTC', $pair->getCurrencySource()->getCode());
        $this->assertEquals('USD', $pair->getCurrencyTarget()->getCode());
        $this->assertEqualsWithDelta(36770.0433, $pair->getExchangeRate(), 0.0001);
    }
}
