<?php

namespace App\Tests\ExchangeRateSource\Parser;

use App\Dto\CurrencyPair;
use App\ExchangeRateSource\Parser\EcbParser;
use PHPUnit\Framework\TestCase;

class EcbParserTest extends TestCase
{
    public function testParse()
    {
        $sample = <<<SAMPLE
<?xml version="1.0" encoding="UTF-8"?>
<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
	<gesmes:subject>Reference rates</gesmes:subject>
	<gesmes:Sender>
		<gesmes:name>European Central Bank</gesmes:name>
	</gesmes:Sender>
	<Cube>
		<Cube time='2021-01-15'>
			<Cube currency='USD' rate='1.2123'/>
			<Cube currency='JPY' rate='125.7423'/>
			<Cube currency='BGN' rate='1.9558'/>
	    </Cube>
	</Cube>
</gesmes:Envelope>
SAMPLE;
        $parser = new EcbParser();
        $pairs = $parser->parse($sample);
        $this->assertCount(3, $pairs);
        $this->assertPair('EUR', 'USD', 1.2123, $pairs[0]);
        $this->assertPair('EUR', 'JPY', 125.7423, $pairs[1]);
        $this->assertPair('EUR', 'BGN', 1.9558, $pairs[2]);
    }

    private function assertPair(string $currencySource, string $currencyTarget, float $exchangeRate, CurrencyPair $pair)
    {
        $this->assertEquals($currencySource, $pair->getCurrencySource()->getCode());
        $this->assertEquals($currencyTarget, $pair->getCurrencyTarget()->getCode());
        $this->assertEqualsWithDelta($exchangeRate, $pair->getExchangeRate(), 0.0001);
    }
}
